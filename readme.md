# *How to write a descriptor for usb hid*

## _usb hid refrence_


[*_Refrence Page_*](http://www.usb.org/developers/hidpage#HID Descriptor Tool)


[*_HID Descriptor Tool_*](http://www.usb.org/developers/hidpage/dt2_4.zip)

## _Good example_

[*_AN 249_*](https://www.silabs.com/documents/public/application-notes/AN249.pdf)

_also my descriptor (BMS.h) can help!!_

## _st application_

[*_USB HID Demonstrator_*](http://www.st.com/content/st_com/en/products/development-tools/software-development-tools/stm32-software-development-tools/stm32-utilities/stsw-stm32084.html)
